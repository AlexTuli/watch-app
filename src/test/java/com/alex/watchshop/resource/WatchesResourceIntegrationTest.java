package com.alex.watchshop.resource;

import com.alex.watchshop.json.Json;
import com.alex.watchshop.repository.DiscountRepository;
import com.alex.watchshop.repository.WatchRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class WatchesResourceIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private DiscountRepository discountRepository;
    @Autowired
    private WatchRepository watchRepository;
    @Autowired
    private TestRestTemplate restTemplate;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void should_calculate_prices() throws JsonProcessingException {
        // given
        var rolex = watchRepository.findByName("Rolex");
        var swatch = watchRepository.findByName("Swatch");

        var request = Json.object()
            .set("watches", Json.object()
                .put(rolex.getId().toString(), "3")
                .put(swatch.getId().toString(), "5"));

        // when
        var response = restTemplate.postForEntity("http://localhost:" + port + "/watch/checkout", request,
            String.class);

        // then
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(objectMapper.readTree(response.getBody()).get("totalPrice").asLong())
            .isEqualTo(450L);
    }

    @Test
    void should_return_list_of_available_watches() throws JsonProcessingException {
        // when
        var response = restTemplate.getForEntity("http://localhost:" + port + "/watch", String.class);

        // then
        final var watches = objectMapper.readTree(response.getBody()).get("watches");
        assertThat(watches).hasSize(4);
    }

    @Test
    void should_throw_error_when_non_existing_id_is_sent() throws JsonProcessingException {
        // given
        var casio = watchRepository.findByName("Casio");

        var request = Json.object()
            .set("watches", Json.object()
            .put(Long.toString(casio.getId() * 2), "3"));

        // when
        var response = restTemplate.postForEntity("http://localhost:" + port + "/watch/checkout", request,
            String.class);

        // then
        assertThat(response.getStatusCodeValue()).isEqualTo(404);
    }

    @Test
    void should_return_zero_when_no_watches_are_sent() throws JsonProcessingException {
        // given
        var request = objectMapper.createObjectNode()
            .set("watches", objectMapper.createObjectNode());

        // when
        var response = restTemplate.postForEntity("http://localhost:" + port + "/watch/checkout", request,
            String.class);

        // then
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(objectMapper.readTree(response.getBody()).get("totalPrice").asLong())
            .isEqualTo(0L);
    }
}
