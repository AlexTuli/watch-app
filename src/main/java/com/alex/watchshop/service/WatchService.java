package com.alex.watchshop.service;

import com.alex.watchshop.exception.NotFoundException;
import com.alex.watchshop.model.Watch;
import com.alex.watchshop.repository.WatchRepository;
import com.alex.watchshop.request.BuyWatchRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

import static com.alex.watchshop.service.WatchesPriceCalculator.calculatePrice;
import static java.math.BigDecimal.ZERO;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.toSet;

@Service
public class WatchService {

    private final WatchRepository watchRepository;

    @Autowired
    public WatchService(WatchRepository watchRepository) {
        this.watchRepository = watchRepository;
    }

    public List<Watch> getWatchesList() {
        return watchRepository.findAll();
    }

    public BigDecimal buy(BuyWatchRequest watchesToBuy) {
        if (watchesToBuy.watchIds().isEmpty()) {
            return ZERO;
        }
        final var watches = watchRepository.findAllById(watchesToBuy.watchIds());
        final var fetchedIds = watches.stream().map(Watch::getId).collect(toSet());
        watchesToBuy.watchIds().stream().filter(not(fetchedIds::contains))
            .findFirst().ifPresent(id -> {
            throw new NotFoundException(Watch.class, id);
        });

        return watches.stream()
            .map(watch -> calculatePrice(watch, watchesToBuy.amountFor(watch.getId())))
            .reduce(ZERO, BigDecimal::add);
    }

}
