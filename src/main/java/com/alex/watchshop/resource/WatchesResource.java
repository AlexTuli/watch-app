package com.alex.watchshop.resource;

import com.alex.watchshop.model.Watch;
import com.alex.watchshop.request.BuyWatchRequest;
import com.alex.watchshop.service.WatchService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.alex.watchshop.json.Json.object;
import static com.alex.watchshop.json.Json.toJsonArray;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/watch")
public class WatchesResource {

    private final WatchService watchService;

    @Autowired
    public WatchesResource(WatchService watchService) {
        this.watchService = watchService;
    }

    @GetMapping
    public @ResponseBody String watches() {
        return object().set("watches", toJsonArray(watchService.getWatchesList().stream()
            .map(WatchesResource::toJson)
            .collect(toList())))
            .toString();
    }

    @PostMapping(value = "/checkout", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public @ResponseBody String checkout(@RequestBody BuyWatchRequest request) {
        return object()
            .put("totalPrice", watchService.buy(request))
            .toString();
    }

    private static ObjectNode toJson(Watch watch) {
        final var watchJson = object().put("name", watch.getName())
            .put("price", watch.getPrice())
            .put("id", watch.getId());
        watch.getDiscount().ifPresent(discount -> watchJson.set("discount", object()
            .put("amount", discount.getDiscountAmount().toString())
            .put("count", discount.getCount())));
        return watchJson;
    }
}
