package com.alex.watchshop.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Entity
@Table(
    name = "watches",
    indexes = {
        @Index(columnList = "name", name = "watch_name_idx", unique = true)
    }
)
public class Watch {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private BigDecimal price;
    @ManyToOne
    private Discount discount;

    public Watch() {

    }

    private Watch(String name, BigDecimal price, Discount discount) {
        this.name = name;
        this.price = price;
        this.discount = discount;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Optional<Discount> getDiscount() {
        return ofNullable(discount);
    }

    public static Watch watch(String name, BigDecimal price) {
        return new Watch(name, price, null);
    }

    public static Watch watch(String name, BigDecimal price, Discount discount) {
        return new Watch(name, price, discount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Watch watch = (Watch) o;
        return Objects.equals(id, watch.id) && Objects.equals(name, watch.name)
            && Objects.equals(price, watch.price) && Objects.equals(discount, watch.discount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, discount);
    }
}
