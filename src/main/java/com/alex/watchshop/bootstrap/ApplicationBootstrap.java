package com.alex.watchshop.bootstrap;

import com.alex.watchshop.repository.DiscountRepository;
import com.alex.watchshop.repository.WatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static com.alex.watchshop.model.Discount.discount;
import static com.alex.watchshop.model.Watch.watch;

@Component
public class ApplicationBootstrap {

    private final DiscountRepository discountRepository;
    private final WatchRepository watchRepository;

    @Autowired
    public ApplicationBootstrap(DiscountRepository discountRepository,
                                WatchRepository watchRepository) {
        this.discountRepository = discountRepository;
        this.watchRepository = watchRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void bootstrapData() {
        final var firstDiscount = discountRepository.save(discount(3, BigDecimal.valueOf(200)));
        final var secondDiscount = discountRepository.save(discount(2, BigDecimal.valueOf(120)));

        final var rolex = watchRepository.save(watch("Rolex", BigDecimal.valueOf(100), firstDiscount));
        final var michael = watchRepository.save(watch("Michael Kors", BigDecimal.valueOf(80), secondDiscount));
        final var swatch = watchRepository.save(watch("Swatch", BigDecimal.valueOf(50)));
        final var casio = watchRepository.save(watch("Casio", BigDecimal.valueOf(30)));
    }
}
