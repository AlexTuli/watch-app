package com.alex.watchshop.repository;

import com.alex.watchshop.model.Watch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WatchRepository extends JpaRepository<Watch, Long> {

    Watch findByName(String name);

}
