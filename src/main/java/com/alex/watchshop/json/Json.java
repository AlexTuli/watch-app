package com.alex.watchshop.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.List;

public final class Json {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static ObjectNode object() {
        return OBJECT_MAPPER.createObjectNode();
    }

    public static ArrayNode toJsonArray(List<ObjectNode> nodes) {
        return OBJECT_MAPPER.createArrayNode().addAll(nodes);
    }
}
