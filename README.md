Watch shop app.

App has endpoint to check out watches:
**POST /watch/checkout**
Request example:

{
  "watches": {
    "watchIdLong": watchesAmountLong
  }
}

The response example:

{
  "totalPrice": long
}

To get a watch id, endpoint GET /watch was introduced that returns all watches and possible discounts for them

To build and run the app you need to do the following:
1. run mvn clean install
2. Locate to /target
3. run java -jar watch-shop-0.0.1-SNAPSHOT.jar

I would improve the next things:

* Created migration scripts that will create a proper DB scheme
  
* Replaced H2 with PostgreSQL
  
* Replaced SpringData (that uses hibernate) by jooq to allow stricter control over the DB layer

* Added pagination for GET and search parameters

